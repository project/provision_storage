<?php

/**
 * Implements hook_drush_load().
 *
 * Checks if the corresponding Hosting Feature is installed and enabled.
 */
function provision_storage_drush_load() {
  $features = drush_get_option('hosting_features', array());
  return array_key_exists('storage', $features) && $features['storage'];
}
